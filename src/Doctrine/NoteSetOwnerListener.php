<?php

namespace App\Doctrine;

use App\Entity\Note;
use Symfony\Component\Security\Core\Security;

class NoteSetOwnerListener
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function prePersist(Note $note)
    {
//        if ($note->getOwner()) {
//            return;
//        }

        if ($this->security->getUser()) {
            $note->setOwner($this->security->getUser());
        }
    }
}
