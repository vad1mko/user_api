<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\NoteRepository;
use App\Validator\IsValidOwner;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     security="is_granted('ROLE_USER')",
 *     collectionOperations={"post"},
 *     itemOperations={"get"},
 *     normalizationContext={"groups"={"note:read"}, "swagger_definition_name"="Read"},
 *     denormalizationContext={"groups"={"note:write"}, "swagger_definition_name"="Write"},
 * )
 * @ORM\Entity(repositoryClass=NoteRepository::class)
 * @ORM\EntityListeners({"App\Doctrine\NoteSetOwnerListener"})
 */
class Note
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @var string $text The text of the note.
     * @ORM\Column(type="string", length=255)
     * @Groups({"note:read", "note:write", "user:read"})
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=2,
     *     max=255,
     *     maxMessage="Input your text in 255 chars or less"
     * )
     */
    private string $text;

    /**
     * @var DateTimeInterface $date The date and time when you need to be reminded.
     * @ORM\Column(type="datetime")
     * @Groups({"note:read", "note:write", "user:read"})
     * @Assert\NotBlank()
     */
    private DateTimeInterface $date;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="notes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"note:read"})
     * @IsValidOwner()
     */
    private ?User $owner;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getText().' - '.$this->getDate()->format('H:i:s d:m');
    }
}
