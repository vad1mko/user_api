<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user1 = $this->createUser('user1', 'user1pass', 'user1@mail.com', ['ROLE_USER']);
        $user2 = $this->createUser('user2', 'user2pass', 'user2@mail.com', ['ROLE_USER']);
        $admin = $this->createUser('admin', 'adminpass', 'admin@mail.com', ['ROLE_ADMIN']);

        $manager->persist($user1);
        $manager->persist($user2);
        $manager->persist($admin);
//        $manager->flush();
    }

    private function createUser(string $username, string $password, string $email, array $roles): User
    {
        $user = new User();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setRoles($roles);

        $password = $this->encoder->encodePassword($user, $password);
        $user->setPassword($password);

        return $user;
    }
}
