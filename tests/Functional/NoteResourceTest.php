<?php

namespace App\Tests\Functional;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\User;

class NoteResourceTest extends ApiTestCase
{

    /*public function testPostNotesWithoutLogin()
    {
        $client = static::createClient();

        $client->request('POST', '/api/notes', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [],
        ]);

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }*/

    public function testCreateUser()
    {
        $client = static::createClient();

        $client->request('POST', '/api/users', [
            'headers' => ['CONTENT_TYPE' => 'application/json'],
            'json' => [
                'email' => 'testuser@mail.com',
                'username' => 'testuser',
                'password' => '$argon2id$v=19$m=65536,t=4,p=1$95tXYOoMHdiOWdhetjASrA$T4Ib+EeAKUAQ5NnsIZzD9D7bpe6zE2MCvZsfSS19gh8',
            ],
        ]);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testLoginUserAndCreateNote()
    {
        $client = static::createClient();

        $client->request('POST', '/login', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'email' => 'testuser@mail.com',
                'password' => 'testuser',
            ],
        ]);

        $this->assertEquals(204, $client->getResponse()->getStatusCode());

        $date = date_create();
        $client->request('POST', '/api/notes', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'text' => 'tincidunt eget nullam non nisi est sit',
                'date' => '2020-12-31T05:56:31',
            ],
        ]);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

    }

    /*public function testLogoutUser()
    {
        $client = static::createClient();

        $client->request('GET', '/logout');

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }*/

}
